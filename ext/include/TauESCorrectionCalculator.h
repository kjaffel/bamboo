#include <ROOT/RVec.hxx>
#include <correction.h>

namespace {
class PtMassVariations {
public:
    PtMassVariations() = default;
    using col_t = ROOT::VecOps::RVec<float>;
     
    PtMassVariations(std::size_t n, const col_t& pt, const col_t& mass): 
    m_pt(n, pt), m_mass(n, mass) {}

    const col_t& pt(std::size_t i) const { return m_pt[i]; }
    const col_t& mass(std::size_t i) const { return m_mass[i]; }

    std::vector<col_t> m_pt;
    std::vector<col_t> m_mass;
};
}

class TauESCorrectionCalculator {
public:
    TauESCorrectionCalculator() = default;
    using col_t = ROOT::VecOps::RVec<float>;
    using col_it = ROOT::VecOps::RVec<int>;
    using result_t = ::PtMassVariations;
    
    TauESCorrectionCalculator(std::string json_path) {
        auto cset = correction::CorrectionSet::from_file(json_path);
        m_tauCorr = cset->at("tau_energy_scale");
    }
    std::vector<std::string> available(const std::string& attr = {}) const {
        return {"nominal", "tauScaleup", "tauScaledown"};
    }
    result_t produce(const col_t& tau_pt, const col_t& tau_mass,const col_t& tau_eta, const col_it& tau_decayMode, const col_it& tau_genPartFlav, const std::string tauTaggerName) const {
        const std::size_t nTau = tau_pt.size();
        result_t out {3, tau_pt, tau_mass};
        for (std::size_t iTau = 0; iTau < nTau; iTau++) {
            const auto pt = tau_pt[iTau];
            const auto eta = tau_eta[iTau];
            const auto decay = tau_decayMode[iTau];
            const auto gen = tau_genPartFlav[iTau];
            if (!(decay < 0 || (decay > 2 && decay < 10) || decay > 11)) {
                const auto corr = m_tauCorr->evaluate({pt, eta, decay, gen, tauTaggerName,"nom"});
                const auto corr_up = m_tauCorr->evaluate({pt, eta, decay, gen, tauTaggerName, "up"});
                const auto corr_down = m_tauCorr->evaluate({pt, eta, decay, gen, tauTaggerName,"down"});
                out.m_pt[0][iTau] *= corr;
                out.m_mass[0][iTau] *= corr;
                out.m_pt[1][iTau] *= corr_up;
                out.m_mass[1][iTau] *= corr_up;
                out.m_pt[2][iTau] *= corr_down;
                out.m_mass[2][iTau] *= corr_down;
            }
        }
        return out;
    }
    
private:
    correction::Correction::Ref m_tauCorr;
};
