import os.path
import subprocess

import pkg_resources
import pytest

pytest.importorskip("dask.distributed")


def get_dask_distributed_version():
    return [int(v) for v in pkg_resources.get_distribution("distributed").version.split(".")]


dask_version = get_dask_distributed_version()
if dask_version[0] < 2022 or (dask_version[0] == 2022 and dask_version[1] < 8):
    pytest.skip("Dask version should be >= 2022.8", allow_module_level=True)

from bamboo.root import ROOT, _rootVersion_split  # noqa
if _rootVersion_split[0] == 6 and _rootVersion_split[1] < 28:
    pytest.skip("ROOT version should be >= 6.28", allow_module_level=True)

from test_bamboorun import pytestmark  # noqa

testData = os.path.join(os.path.dirname(__file__), "data")
examples = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "examples")


def test_bambooRun_minimalnanozmm_distRDF_dask_local_parallel():
    assert subprocess.run([
        "bambooRun",
        "--module={}:MinimalNanoZMuMu".format(os.path.join(examples, "nanozmumu.py")),
        "--distrdf-be=dask_local",
        "--distributed=parallel",
        "--backend=distributed",
        "--envConfig=../examples/distributed.ini",
        os.path.join(examples, "test1.yml"),
        "--output=test_distRDF_parallel"]).returncode == 0


def test_bambooRun_minimalnanozmm_distRDF_dask_local_sequential():
    assert subprocess.run([
        "bambooRun",
        "--module={}:MinimalNanoZMuMu".format(os.path.join(examples, "nanozmumu.py")),
        "--distrdf-be=dask_local",
        "--distributed=sequential",
        "--backend=distributed",
        "--envConfig=../examples/distributed.ini",
        os.path.join(examples, "test1.yml"),
        "--output=test_distRDF_sequential"]).returncode == 0


def test_bambooRun_nanozmm_distRDF_dask_local_parallel():
    assert subprocess.run([
        "bambooRun",
        "--module={}:NanoZMuMu".format(os.path.join(examples, "nanozmumu.py")),
        "--distrdf-be=dask_local",
        "--distributed=parallel",
        "--backend=distributed",
        "--envConfig=../examples/distributed.ini",
        os.path.join(examples, "test1.yml"),
        "--output=test_distRDF_parallel_skimming"]).returncode == 0


def test_bambooRun_nanozmm_distRDF_dask_local_sequential():
    assert subprocess.run([
        "bambooRun",
        "--module={}:NanoZMuMu".format(os.path.join(examples, "nanozmumu.py")),
        "--distrdf-be=dask_local",
        "--distributed=sequential",
        "--backend=distributed",
        "--envConfig=../examples/distributed.ini",
        os.path.join(examples, "test1.yml"),
        "--output=test_distRDF_sequential_skimming"]).returncode == 0
